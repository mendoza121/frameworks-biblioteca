import { createRouter, createWebHistory } from 'vue-router'
import Home from '../views/Home.vue'

const routes = [
  {
    path: '/',
    name: 'home',
    component: Home
  },
  {
    path: '/sesion',
    name: 'sesion',
    component: () => import(/* webpackChunkName: "general" */ '../views/Sesion.vue')
  },
  {
    path: '/registro',
    name: 'registro',
    component: () => import(/* webpackChunkName: "general" */ '../views/Registro.vue')
  },
  {
    path: '/inicio',
    name: 'inicio',
    component: () => import(/* webpackChunkName: "general" */ '../views/Inicio.vue')
  },
  {
    path: '/busqueda',
    name: 'busqueda',
    component: () => import(/* webpackChunkName: "general" */ '../views/Busqueda.vue')
  },
  {
    path: '/categorias',
    name: 'categorias',
    component: () => import(/* webpackChunkName: "general" */ '../views/Categorias.vue')
  },
  {
    path: '/multas',
    name: 'multas',
    component: () => import(/* webpackChunkName: "general" */ '../views/Multas.vue')
  },
  {
    path: '/ayuda',
    name: 'ayuda',
    component: () => import(/* webpackChunkName: "general" */ '../views/Ayuda.vue')
  },
  {
    path: '/cuenta',
    name: 'cuenta',
    component: () => import(/* webpackChunkName: "general" */ '../views/Cuenta.vue')
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router